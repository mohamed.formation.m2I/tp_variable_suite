const prompt = require("prompt-sync")();

//Exercice 10 
console.log("Exercice 10 : ");
console.log(" ");
const email = "contact@gmail.fr";
const mpd = "leLundiAuSoleil";

var userEmail = prompt("Veuillez saisir votre email : ");
var userMdp = prompt("Veuillez saisir votre mot de passe : ");

if (userEmail == email) {

    if (userMdp == mpd) {
        console.log("Bienvenue dans votre espace sécurisé")
    } else {
        console.log("Mot de passe incorrect");
    }
} else {
    console.log("Email non reconnu");
}


// Exercice 13

function categorieEnfant() {

    var age = Number(prompt("Quel est l'age de l'enfant ? "));

    console.log(age);

    if (age >= 12) {
        console.log(age);
        console.log("Votre enfant sera dans la catégorie Cadet")
    } else if (age >= 11) {

        console.log("Votre enfant sera dans catégorie Minime")

    } else if (age >= 9) {

        console.log("Votre enfant sera dans catégorie Pupille")
    } else if (age >= 7) {

        console.log("Votre enfant sera dans catégorie Poussin")
    }
    else if (age >= 3) {

        console.log("Votre enfant sera dans catégorie Baby")
    } else {
        console.log("Votre enfant est trop jeune")
    }
}

categorieEnfant();

//Exercice 14
const AB = prompt("Veuillez saisir le longueur du segment AB : ")
const BC = prompt("Veuillez saisir le longueur du segment BC : ")
const CA = prompt("Veuillez saisir le longueur du segment CA : ")


if (AB == BC) {

    if (BC == CA) {
        console.log("Le triangle est equilatéral");
    }
    else {
        console.log("Le triangle est isocele en B");
    }
} else if (CA == BC) {

    console.log("Le triangle est isocele en C");

} else if (CA == AB) {

    console.log("Le triangle est isocele en A");

} else {
    console.log("Le triangle n'est ni isocel ni equilateral");

}


//#region 
// Exercice 15 

let poids = prompt("Quelle est votre poids ? ")
let taille = prompt("Quelle est votre taille ? ")
if ((taille >= 145 && taille <= 169 && poids >= 43 && poids <= 47) || (taille >= 145 && taille <= 166 && poids >= 48 && poids <= 53) || (taille >= 145 && taille <= 163 && poids >= 54 && poids <= 59) || (taille >= 145 && taille <= 160 && poids >= 60 && poids <= 65)) {
    console.log("taille 1")
} else if ((taille >= 169 && taille <= 178 && poids >= 48 && poids <= 53) || (taille >= 169 && taille <= 175 && poids >= 54 && poids <= 59) || (taille >= 163 && taille <= 172 && poids >= 60 && poids <= 65) || (taille >= 160 && taille <= 169 && poids >= 66 && poids <= 71)) {
    console.log("taille 2")
} else if ((taille >= 178 && taille <= 183 && poids >= 54 && poids <= 59) || (taille >= 175 && taille <= 183 && poids >= 60 && poids <= 65) || (taille >= 172 && taille <= 183 && poids >= 66 && poids <= 71) || (taille >= 163 && taille <= 183 && poids >= 72 && poids <= 77)) {
    console.log("taille 3")
}
else {
    console.log("error taille")
}

//#endregion


// Exercice 16

// Déclaration de variables
let indemnite = 0;

// Récupération et stockage des saisie utilisateur
let salaire = parseInt(prompt("Veuillez saisir le dernier :"));
let anciennete = parseInt(prompt("Veuillez saisir l'ancienneté :"));
let age = parseInt(prompt("Veuillez saisir l'age :"));

// Etablissement d'une structure conditionnelle afin de déterminer le montant de la prime
if (anciennete >= 1 && anciennete <= 10) {
    indemnite += anciennete * salaire / 2;
}
if (anciennete > 10) {
    indemnite += 10 * salaire / 2;
    indemnite += (anciennete - 10) * salaire;
}

if (age > 45) {

    if (age < 50) {
        indemnite += 2 * salaire;
    } else {
        indemnite += 5 * salaire;
    }

    //indemnite += age < 50 ? 2 * salaire : 5 * salaire;
}

console.log(` Le montant de l'indemnité pour un salaire de ${salaire}€ et un age de ${age} ans
avec une ancienneté de ${anciennete}  s'éléve à ${indemnite}€.`);

// Exercice 17

// Déclaration de variables
let revenus = 0,
    nbAdulte = 0,
    nbEnfant = 0,
    nbParts = 0,
    revenuImposable = 0,
    montantImpot = 0;


// Récupération et stockage des saisie utilisateur
revenus = parseInt(prompt("Veuillez saisir Le revenu fical de référence :"));
nbAdulte = parseInt(prompt("Veuillez saisir le nombre d'adultes :"));
nbEnfant = parseInt(prompt("Veuillez saisir le nombre d'enfants :"));

// Calcul du nombre de part
if (nbEnfant <= 2)
    nbParts = nbAdulte + nbEnfant * 0.5;
else
    nbParts = nbAdulte + nbEnfant - 1;

// // Calcul du nombre part en sugar syntaxe
// nbParts = nbEnfant <= 2 ? nbAdulte + nbEnfant * 0.5 : nbAdulte + nbEnfant - 1;

// Calcul du montant des l'impôt
revenuImposable = revenus / nbParts;

if (revenuImposable >= 10085 && revenuImposable <= 25710)
    montantImpot = Math.round((revenuImposable - 10084) * 0.11);
else if (revenuImposable >= 25711 && revenuImposable <= 73516)
    montantImpot = Math.round((revenuImposable - 25711) * 0.30 + (25711 - 10085) * 0.11);
else if (revenuImposable >= 73517 && revenuImposable <= 158122)
    montantImpot = Math.round((revenuImposable - 73516) * 0.41 + (73516 - 25710) * 0.30 + (25711 - 10085) * 0.11);
else if (revenuImposable >= 158123)
    montantImpot = Math.round((revenuImposable - 158122) * 0.45 + (158122 - 73516) * 0.41 + (73516 - 25710) * 0.30 + (25711 - 10085) * 0.11);

montantImpot *= nbParts;

console.log(montantImpot);

console.log(`Le montant de l'impôt pour un revenu fiscal de ${revenus}€ avec ${nbAdulte} adulte(s)
et ${nbEnfant} enfant(s) s'éléve à ${montantImpot}€.`);



// Exercice 21 : 

for (let index = 0; index <= 10; index++) {
    console.log("." + index);

}

console.log("Super, je sais compter jusqu'à 10!");

// Exercice 22 : 

var nombre = 0;
do {
    nombre = prompt("Entrer un nombre compris entre 10 et 20. ");
    if (nombre < 10) {
        console.log("Plus grand !");
    }
    else if (nombre > 20) {
        console.log("Plus petit !");
    }
} while (nombre < 10 || nombre > 20)


// Exercice 23

let nombre = Number(prompt("Donnez-moi un nombre :  ? "));
let resultat = 0;

console.log("Table de " + nombre);

for (i = 1; i <= 10; i++) {
    resultat = nombre * i;
    console.log(nombre + " x " + i + " = " + resultat);
}

// Exercice 24

let prixAchat = 0;
let paiement;
let prix;
let prixProduit
do {
    prixProduit = prompt("Quel est le prix de votre article ? Fin saisir avec 0  ");
    prixAchat = parseInt(prixAchat) + parseInt(prixProduit);
} while (prixProduit != 0)
paiement = prompt("Quelle somme donnez-vous ? ");
prix = paiement - prixAchat;
while (prix < 0) {
    paiement = prompt("Somme insuffisante, veuillez recommencer le paiement. ");
    prix = paiement - prixAchat;
}
while (prix >= 10) {
    prix = prix - 10;
    console.log("10 euros ");
}
while (prix >= 5) {
    prix = prix - 5;
    console.log("5 euros ");
}
for (let i = 1; i <= prix; i++) {
    console.log("1 euro ");
}


// Exercice 25

let plusGrand, nombre;

for (i = 0; i < 20; i++) {
    nombre = Number(prompt("Donnez moi le nombre " + (i + 1) + " : "));

    if (i == 0) {
        plusGrand = nombre;

    }
    if (nombre > plusGrand) {
        plusGrand = nombre;
    }
}
console.log("Le plus grand des nombres est le : " + plusGrand);


